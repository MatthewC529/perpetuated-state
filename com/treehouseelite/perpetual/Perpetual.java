package com.treehouseelite.perpetual;

import com.badlogic.gdx.Game;
import com.treehouseelite.perpetual.screens.Level;

public class Perpetual extends Game {

	@Override
	public void create() {
		Ref.GAME = this;
		this.setScreen(new Level());
	}
	
	
}
