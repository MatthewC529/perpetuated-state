package com.treehouseelite.perpetual.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

public class SimpleActor {
	
	protected BodyDef bDef;
	protected Body actor;
	protected World world;
	protected Texture skin;
	
	protected Vector2 position = new Vector2();
	protected Vector2 CenterOfMass = new Vector2();
	
	private Array<Fixture> fixtures = new Array<Fixture>();
	
	public static Array<SimpleActor> actors = new Array<SimpleActor>();
	public static Array<Obstacle> obstacles = new Array<Obstacle>();
	public static Array<Obstacle> staticWalls = new Array<Obstacle>();
	
	public SimpleActor(int x, int y, BodyType type, World world, Texture tex){
		
		bDef = new BodyDef();
		bDef.position.set(x, y);
		bDef.type = type;
		this.world = world;
		skin = tex;
		actor = world.createBody(bDef);
		
		if(this instanceof Player){
			FixtureDef fixDef = new FixtureDef();
			CircleShape circle = new CircleShape();
			circle.setRadius(6.0f);
			
			fixDef.shape = circle;
			fixDef.friction = 0;
			fixDef.density = 0.5f;
			fixDef.restitution = 0.6f;
			
			Fixture main = actor.createFixture(fixDef);
			fixtures.add(main);
			circle.dispose();
		}
		
		if(this instanceof Obstacle){
			FixtureDef fixDef = new FixtureDef();
			PolygonShape shape = new PolygonShape();
			
			shape.setAsBox(skin.getWidth()/2, skin.getHeight()/2);
			fixDef.shape = shape;
			fixDef.friction = 0.3f;
			fixDef.density = 1.0f;
			fixDef.restitution = 0f;
			
			Fixture main = actor.createFixture(fixDef);
			fixtures.add(main);
			shape.dispose();
		}
		
		position.set(x, y);
		
	}
	
	public void update(float delta){
		position = actor.getPosition();
	}
	
	public Vector2 getPosition(){
		return position;
	}
	
	public Vector2 getVelocity(){
		return actor.getLinearVelocity();
	}
	
	public Body getBody(){
		return actor;
	}
	
	public Texture getTexture(){
		return skin;
	}
	
	public Array<Fixture> getFixtures(){
		return fixtures;
	}
	
	public void drawMe(SpriteBatch batch){
		batch.draw(skin, actor.getPosition().x-16, actor.getPosition().y-16, skin.getWidth(), skin.getHeight());
		
	}
	
	public void applyImpulse(float impX, float impY){
		applyImpulse(new Vector2(impX, impY));
	}
	
	public void applyImpulse(Vector2 imp){
		this.actor.applyLinearImpulse(imp, CenterOfMass, true);
	}
	public void applyForce(float fX, float fY){
		applyForce(new Vector2(fX, fY));
	}
	public void applyForce(Vector2 f){
		this.actor.applyForce(f, CenterOfMass, true);
	}

	
	public static void bleach(){
		actors.clear();
	}
	
}
