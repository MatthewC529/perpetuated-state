package com.treehouseelite.perpetual.actors;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.treehouseelite.perpetual.Ref;
import com.treehouseelite.perpetual.util.InputState;

public class Player extends SimpleActor{
	
	private ParticleEffect particles = new ParticleEffect();
	private boolean supersonic = false;
	
	public Player(int x, int y, World world) {
		super(x, y, BodyType.DynamicBody, world, new Texture(Ref.PLAYERTEX));
		particles.loadEmitters(Ref.PLAYER_PARTICLE);
		particles.loadEmitterImages(Ref.PARTICLE_IMAGES_DIR);
		actor.setLinearVelocity(1e6f, 0);
	}
	
	@Override
	public void update(float delta){
		position = this.actor.getPosition();
		particles.setPosition(position.x, position.y);
		CenterOfMass = new Vector2(position.x + skin.getWidth()/2, position.y + skin.getHeight()/2);
		applyImpulse(Ref.XGRAVITY,0);
		
		if(actor.getLinearVelocity().x > 10 && !supersonic){
			particles.getEmitters().get(2).setContinuous(true);
			supersonic = true;
		}else{
			particles.getEmitters().get(2).setContinuous(false);
			supersonic = false;
		}
	}
	
	public void updateInput(InputState state, float delta){
		if(state.isKeyPressed(Keys.ESCAPE)){
			Gdx.app.exit();
		}
		
		if(state.isKeyPressed(Keys.W)){
			applyImpulse(70000,1000000);
			//actor.setLinearVelocity(actor.getLinearVelocity().add(80000, 100000));
		}
		
		if(state.isKeyPressed(Keys.D)){
			applyForce(Ref.XGRAVITY, 0);
		}
		
		if(state.isKeyPressed(Keys.S)){
			applyImpulse(70000, -1000000);
			//actor.setLinearVelocity(actor.getLinearVelocity().add(80000,-100000));
		}
	}
	
	@Override
	public void drawMe(SpriteBatch batch){
		batch.draw(skin, position.x - 8, position.y-8, 16, 16);
	}
	
	public ParticleEffect getParticleEffect(){
		return particles;
	}
}
