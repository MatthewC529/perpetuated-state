package com.treehouseelite.perpetual.actors;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;
import com.treehouseelite.perpetual.Ref;

public class Obstacle extends SimpleActor{
	
	public enum TileType{
		horizontal, vertical, upend, downend, rightend, leftend,
		junction, singular;
	}
	
	public enum Movement{
		stationary, mobile;
	}
	
	private Obstacle(int x, int y, BodyType type, World world, Texture tex) {
		super(x, y, type, world, tex);
	}
	
	public Obstacle(int x, int y, World world, TileType orient){
		this(x, y, BodyType.KinematicBody, world, findTexture(orient));
		obstacles.add(this);
	}
	
	@Override
	public void drawMe(SpriteBatch batch){
		
	}
	
	@Override
	public void update(float delta){
		position = actor.getPosition();
	}
	
	private static Texture findTexture(TileType orient){
		if(orient == TileType.horizontal){
			return new Texture(Ref.TILE_HORIZONTAL);
		}else if(orient == TileType.vertical){
			return new Texture(Ref.TILE_VERTICAL);
		}else if(orient == TileType.junction){
			return new Texture(Ref.TILE_NOSHADE);
		}else if(orient == TileType.singular){
			return new Texture(Ref.TILE_SINGLE);
		}else if(orient == TileType.upend){
			return new Texture(Ref.TILE_UPEND);
		}else if(orient == TileType.downend){
			return new Texture(Ref.TILE_DOWNEND);
		}else if(orient == TileType.rightend){
			return new Texture(Ref.TILE_RIGHTEND);
		}else{
			return new Texture(Ref.TILE_LEFTEND);
		}
	}

}
