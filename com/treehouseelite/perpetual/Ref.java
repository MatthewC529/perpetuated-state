package com.treehouseelite.perpetual;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public final class Ref {
	
	// Basic Info
	public static final String TITLE = "Perpetuated Dream";
	public static final int width = 1024;
	public static final int height = 512;
	public static boolean DEBUG = true;
	public static Perpetual GAME;
	
	public static float XGRAVITY = 1000000;
	public static float YGRAVITY = 100;
	
	//Textures
	public static final FileHandle PLAYERTEX = getFile("player.png");
	public static final FileHandle TILE_NOSHADE = getFile("TNoShade.png");
	public static final FileHandle TILE_HORIZONTAL = getFile("THorizontal.png");
	public static final FileHandle TILE_VERTICAL = getFile("TVertical.png");
	public static final FileHandle TILE_SINGLE = getFile("TSingle.png");
	public static final FileHandle TILE_UPEND = getFile("TUpEnd.png");
	public static final FileHandle TILE_DOWNEND = getFile("TDownEnd.png");
	public static final FileHandle TILE_LEFTEND = getFile("TLeftEnd.png");
	public static final FileHandle TILE_RIGHTEND = getFile("TRightEnd.png");
	public static final FileHandle PLAYER_PARTICLE = getFile("particles/PlayerPE.p");
	public static final FileHandle PLAYER_PARTICLECSW = getFile("particles/PlayerPECSW.p");
	
	public static final FileHandle PARTICLE_IMAGES_DIR = getFile("particles");
	
	private static FileHandle getFile(String relPath){
		return Gdx.files.internal(relPath);
	}
	
}
