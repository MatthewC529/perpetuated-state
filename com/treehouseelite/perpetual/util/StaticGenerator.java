package com.treehouseelite.perpetual.util;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.treehouseelite.perpetual.Ref;
import com.treehouseelite.perpetual.actors.Obstacle;
import com.treehouseelite.perpetual.actors.SimpleActor;
import com.treehouseelite.perpetual.actors.Obstacle.TileType;
import com.treehouseelite.perpetual.actors.Player;
import com.treehouseelite.perpetual.util.exception.StaticOrientationException;

public class StaticGenerator {
	
	private static Array<Body> bodies = new Array<Body>();
	
	public static void createFloor(int x, int y, World world, Orientation orient){
		
		BodyDef floorDef = new BodyDef();
		floorDef.type = BodyType.KinematicBody;
		floorDef.position.set(x, y);
		
		Body floor = world.createBody(floorDef);
		
		PolygonShape floorShape = new PolygonShape();
		if(orient == Orientation.Horizontal){
			populate(orient, x-Ref.width/2-32, y, world);
			bodies.add(floor);
		}else if(orient == Orientation.Vertical){ 
			populate(orient, x, y-Ref.height/2-32, world);
		}else throw new StaticOrientationException();
		
		floor.createFixture(floorShape, 0);
		floorShape.dispose();
	}
	
	public static void updateLocations(Player p, float delta){
		for(Body b: bodies){
			b.setLinearVelocity(p.getVelocity().x, 0);
		}
		for(Obstacle o: SimpleActor.staticWalls){
			if(p.getPosition().x > Ref.width/2)
			o.getBody().setLinearVelocity(p.getVelocity().x, 0);
			o.update(delta);
		}
	}
	
	private static void populate(Orientation orient, int x, int y, World world){
		int tiles;
		if(orient == Orientation.Vertical){
			tiles = (Ref.height*2)/32;
			for(int a = 1; a <= tiles; a++){
				if(a == 1) new Obstacle(x, y+(32*a), world, TileType.upend);
				if(a == tiles)new Obstacle(x, y+(32*a), world, TileType.downend);
				if(a != 1 && a != tiles)new Obstacle(x, y+(32*a), world, TileType.vertical);
			}
		}else{
			tiles = (Ref.width*2)/32;
			Obstacle o = null;
			for(int b = 1; b<=tiles; b++){
				if(b == 1) o = new Obstacle(x+32*b, y, world, TileType.leftend);
				if(b == tiles)o = new Obstacle(x+32*b, y, world, TileType.rightend);
				if(b!= tiles && b!=1) o = new Obstacle(x+32*b, y, world, TileType.horizontal);
				SimpleActor.staticWalls.add(o);
			}
		}
	}
}
