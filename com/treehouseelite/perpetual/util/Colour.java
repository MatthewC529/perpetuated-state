package com.treehouseelite.perpetual.util;

import com.badlogic.gdx.graphics.Color;

public class Colour extends Color {
	
	public int val8;
	
	public Colour(float r, float g, float b, float a){
		super(r, g, b, a);
		deriveValue();
	}
	
	public Colour(Color c){
		super(c);
		deriveValue();
	}
	public  Colour(float r, float g, float b){
		this(r, g, b, 1);
		deriveValue();
	}
	
	public Colour(String hex){
		this(hex, 1);
		deriveValue();
	}
	
	public Colour(String hex, float a){
		super(convertHex(hex,a));
		deriveValue();
	}
	
	private static int convertHex(String hex, float a){
		StringBuilder sb = new StringBuilder(hex);
		if(hex.contains("#")){
			while(sb.length()>7){
				sb.deleteCharAt(sb.lastIndexOf(hex));
				hex = sb.toString();
				hex.trim();
			}
			sb.deleteCharAt(hex.indexOf(Character.valueOf('#')));
			hex = sb.toString();
			hex.trim();
		}
		
		float r = (float)(Integer.parseInt(hex.substring(0, 2), 16)/255f);
		float g = (float)(Integer.parseInt(hex.substring(2, 4), 16)/255f);
		float b = (float)(Integer.parseInt(hex.substring(4, 6), 16)/255f);
		
		return rgba8888(r, g, b, a);
	}
	
	private void deriveValue(){
		val8 = Color.rgba8888(this);
	}
	
}
