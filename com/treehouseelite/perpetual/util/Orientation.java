package com.treehouseelite.perpetual.util;

public enum Orientation {
	
	Horizontal, Vertical, TJunction, ReverseTJ, UpEnd, DownEnd;
	
}
