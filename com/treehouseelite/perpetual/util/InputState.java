package com.treehouseelite.perpetual.util;

public class InputState {

	private boolean[] keyRegister;
	private int mouseX;
	private int mouseY;
	private int scrollAmount = 1;

	public InputState(){
		keyRegister = new boolean[310];
	}

	public void keyDown(int key){
		keyRegister[key] = true;
	}

	public void keyUp(int key){
		keyRegister[key] = false;
	}

	public void scrollAmoung(int sA){
		if(sA == 1 && scrollAmount < 1) scrollAmount = 1;
		else scrollAmount += sA;
	}

	public void storeMouse(int x, int y){
		mouseX = x;
		mouseY = y;
	}

	public boolean isKeyPressed(int key){
		return keyRegister[key];
	}

	public int gMouseX(){return mouseX;}
	public int gMouseY(){return mouseY;}
	public int gScrollAmount(){return scrollAmount;}


}

