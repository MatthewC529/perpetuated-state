package com.treehouseelite.perpetual.util;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.physics.box2d.World;
import com.treehouseelite.perpetual.actors.Obstacle;
import com.treehouseelite.perpetual.actors.Obstacle.TileType;

public class WorldGeneration extends Pixmap {
	
	public static final Colour OBSTACLE = new Colour("#FFFFFF");
	
	int temp;
	public static World phys;
	
	public WorldGeneration(FileHandle file, World world) {
		super(file);
		phys = world;
	}
	
	public WorldGeneration(int width, int height, Format format, World world){
		super(width, height, format);
		phys = world;
	}
	
	public void populate(){
		for (int y = 0; y<this.getHeight();y++){
			for (int x = 0; x<this.getWidth();x++){
				temp = this.getPixel(x, y);
				if(temp == OBSTACLE.val8) new Obstacle(x*32, y*32, phys, TileType.singular);
			}
		}
	}
	
}
