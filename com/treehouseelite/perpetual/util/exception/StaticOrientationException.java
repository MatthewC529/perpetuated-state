package com.treehouseelite.perpetual.util.exception;

import com.badlogic.gdx.utils.GdxRuntimeException;

@SuppressWarnings("serial")
public class StaticOrientationException extends GdxRuntimeException{

	public StaticOrientationException(String message) {
		super(message);
	}
	
	public StaticOrientationException(Throwable t){
		super(t);
	}
	
	public StaticOrientationException(String msg, Throwable t){
		super(msg,t);
	}
	
	public StaticOrientationException(){
		super("Fact: The Code either Forgot to choose an Orientation or You Failed to Use the Orientation Enum!");
	}
}
