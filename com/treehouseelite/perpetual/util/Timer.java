package com.treehouseelite.perpetual.util;

import java.util.ArrayList;

/**
 * A Convenient Utility that allows you to put timers on things without putting down code haphazardly. Pass a time in millis or seconds
 * and the timer will let you know if timeIsUp() when you call that method. Please Wait is a 5 second timer.
 * 
 * @author Matthew Crocco (Treehouse Elite)
 *
 */
public class Timer {

	private long waitTime;
	private long curTime;
	private long targetTime;
	private boolean set = false;

	private final int desiredCapacity = 50;

	private static ArrayList<Timer> timers = new ArrayList<Timer>();

	public Timer(){}

	public void pleaseWait(){

		curTime = System.currentTimeMillis();
		waitTime = 5000;
		targetTime = curTime+waitTime;

		checkList();

		set = true;
		timers.add(this);

	}

	// You want a REALLY fast timer...
	public void microWait(long micros){
		time(micros/1000L);
	}

	// You want a VERY REALLY REALLY REALLY fast timer....
	public void nanoWait(long nanos){
		time(nanos/1000000L);
	}

	//Sorry no femtoWait()...

	/**
	 * Pass in the amount of milliseconds you wish to wait and it sets the timer.
	 * 
	 * @param millis
	 */
	public void time(long millis){
		curTime = System.currentTimeMillis();
		waitTime = millis;

		targetTime = curTime+waitTime;
		checkList();
		set = true;
		timers.add(this);
	}

	/**
	 * Pass in the amount of seconds you wish to wait and this method will set the proper timer.
	 * 
	 * @param seconds
	 */
	public void time(int seconds){
		time(seconds * 1000L);
	}

	/**
	 * Checks if the Timer is Up.
	 * 
	 * @return True if the time has been reached, false if not quite yet.
	 */
	public boolean timeIsUp(){
		curTime = System.currentTimeMillis();
		if(curTime >= targetTime){
			timers.remove(this);
			timers.trimToSize();
			return true;
		}else{
			return false;
		}
	}

	private void checkList(){ //I dont think we will have 50 timers but you know... just in case.
		if(timers.size()>desiredCapacity){
			for(int re = 0; re<timers.size()-desiredCapacity;re++){
				timers.remove(re);
			}
		}
	}

	public boolean isSetUp(){
		return set;
	}

	public void stop(){
		targetTime = System.currentTimeMillis()-1;
	}

}
