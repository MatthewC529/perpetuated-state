package com.treehouseelite.perpetual.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.treehouseelite.perpetual.Ref;
import com.treehouseelite.perpetual.util.InputHandler;
import com.treehouseelite.perpetual.util.InputState;

public class MainMenu implements Screen {
	
	OrthographicCamera cam;
	SpriteBatch batch;
	BitmapFont Scribbles;
	BitmapFont Formal;

	InputHandler Doge;
	InputState Coin;
	
	public MainMenu(){
		
		cam = new OrthographicCamera();
		cam.setToOrtho(true, Ref.width, Ref.height);
		batch = new SpriteBatch();
		Scribbles = new BitmapFont(Gdx.files.internal("fonts/visitor_48px.fnt"));
		Formal = new BitmapFont(Gdx.files.internal("fonts/visitor_24px.fnt"));
		
		Doge = new InputHandler();
		Gdx.input.setInputProcessor(Doge);
	}
	
	@Override
	public void render(float delta) {
		
		Coin = Doge.getState();
		
		if(Coin.isKeyPressed(Keys.ESCAPE)) Gdx.app.exit();
		if(Coin.isKeyPressed(Keys.SPACE)) {
			dispose();
			Ref.GAME.setScreen(new Level());
		}
		
		batch.begin();
			Scribbles.draw(batch, "Perpetuated State", (float) (Ref.width/2 - (Scribbles.getSpaceWidth()*8.5)), Ref.height - 80);
			Formal.draw(batch, "TreeDare #1: Perpetual Motion", (float) (Ref.width/2 - (Scribbles.getSpaceWidth()*8.5)), Ref.height - (90 + Scribbles.getCapHeight()));
			Scribbles.draw(batch, "Space to Start", (Ref.width/2 - (Scribbles.getSpaceWidth() * 6)), Ref.height/2);
		batch.end();
		
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		batch.dispose();
		Formal.dispose();
		Scribbles.dispose();
		
	}

}
