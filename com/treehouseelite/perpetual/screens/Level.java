package com.treehouseelite.perpetual.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.treehouseelite.perpetual.Ref;
import com.treehouseelite.perpetual.actors.Obstacle;
import com.treehouseelite.perpetual.actors.Player;
import com.treehouseelite.perpetual.actors.SimpleActor;
import com.treehouseelite.perpetual.util.Colour;
import com.treehouseelite.perpetual.util.InputHandler;
import com.treehouseelite.perpetual.util.InputState;
import com.treehouseelite.perpetual.util.StaticGenerator;
import com.treehouseelite.perpetual.util.Orientation;
import com.treehouseelite.perpetual.util.WorldGeneration;

public class Level implements Screen{

	World world;
	WorldGeneration Columbus;
	Box2DDebugRenderer debug; 
	
	InputHandler Bit;
	InputState Coin;
	
	OrthographicCamera cam;
	SpriteBatch batch;
	
	Colour clearColor = new Colour(Color.BLACK);
	
	Player Steve;
	
	public Level(){
		
		cam = new OrthographicCamera();
		cam.setToOrtho(false, Ref.width, Ref.height);
		world = new World(new Vector2(Ref.XGRAVITY, -Ref.YGRAVITY), false);
		debug = new Box2DDebugRenderer();
		Steve = new Player(0, 300, world);
		
		Columbus = new WorldGeneration(100, Ref.height/32, Format.RGBA8888, world);
		Columbus.drawPixel(10, 9, WorldGeneration.OBSTACLE.val8);
		Columbus.populate(); //Irony
		
		batch = new SpriteBatch();
		Bit = new InputHandler();
		Gdx.input.setInputProcessor(Bit);
		
		StaticGenerator.createFloor(Ref.width/2, 0, world, Orientation.Horizontal);
		StaticGenerator.createFloor(Ref.width/2, Ref.height-10, world, Orientation.Horizontal);
		//StaticGenerator.createFloor(10, Ref.height/2, world, Orientation.Vertical);
	}
	
	
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(clearColor.r, clearColor.g, clearColor.b, clearColor.a);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		Coin = Bit.getState();
		
		StaticGenerator.updateLocations(Steve, delta);
		Steve.update(delta);
		Steve.updateInput(Coin, delta);
		
		if(Steve.getPosition().x > Ref.width/2) cam.position.x = Steve.getPosition().x;
		cam.update();
		
		batch.setProjectionMatrix(cam.combined);
		batch.begin();
			for(Obstacle o: SimpleActor.obstacles) o.drawMe(batch);
			doChecks(batch, delta);
			Steve.drawMe(batch);
		batch.end();
		
		debug.render(world, cam.combined);
		world.step(1/100000f, 5, 2);
		
	}
	
	private void doChecks(SpriteBatch batch, float delta){
		if(Steve.getBody().getLinearVelocity().x < 1000){
			Steve.getParticleEffect().allowCompletion();
			if(!Steve.getParticleEffect().isComplete()){
				Steve.getParticleEffect().draw(batch, delta);
			}else Steve.getParticleEffect().reset();
		}else{
			
			if(Steve.getParticleEffect().isComplete()) Steve.getParticleEffect().start();
			Steve.getParticleEffect().draw(batch, delta);
		}
	}
	
	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
